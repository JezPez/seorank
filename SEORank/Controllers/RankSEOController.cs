using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SEORank.Models;
using SEORank.Services;

namespace SEORank.Controllers
{
    [Route("api/[controller]")]
    public class RankSEOController : Controller
    {
        private ISearchService _searchService;
        public RankSEOController(ISearchService searchService)
        {
            _searchService = searchService;
        }

        [HttpGet("")]
        public async Task<RankSEOResult> GetSEORank(string providerName, string targetUrlPart, string searchQuery)
        {
            var validationResult = this.ValidateSearch(providerName, targetUrlPart, searchQuery);
            if (!validationResult.IsValid)
            {
                return new RankSEOResult { Error = validationResult.Error };
            }

            var initialRequest = _searchService.GetInitialRequest(providerName, searchQuery);
            var sites = await _searchService.SearchAndRankSites(providerName, targetUrlPart, searchQuery);

            return new RankSEOResult
            {
                RankedSites = sites,
                InitialRequest = initialRequest
            };
        }

        /// <summary>
        /// Validates the search input
        /// </summary>
        /// <param name="targetUrl">The Url part to find in search results.</param>
        /// <param name="searchQuery">The search term or keywords to use to search.</param>
        /// <returns>The error message if any.</returns>
        private ValidationResult ValidateSearch(string providerName, string target, string searchQuery)
        {
            if (string.IsNullOrEmpty(providerName))
            {
                return new ValidationResult("The given providerName must not be empty");
            }
            else if (string.IsNullOrEmpty(target))
            {
                return new ValidationResult("The given target URL part must not be empty");
            }
            else if (string.IsNullOrEmpty(searchQuery))
            {
                return new ValidationResult("The given search query must not be empty");
            }

            return new ValidationResult();
        }
    }
}
