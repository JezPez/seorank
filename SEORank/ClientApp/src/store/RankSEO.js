const requestRankSEOType = 'REQUEST_RANKSEO';
const receiveRankSEOType = 'RECEIVE_RANKSEO';
const initialState = { rankSEOResult: { rankedSites: [] }, isLoading: false };

export const actionCreators = {
  requestRankSEO: (provider, urlPart, search) => async (dispatch, getState) => {
    if (provider === getState().rankSEOState.provider && 
     urlPart === getState().rankSEOState.urlPart && 
     search === getState().rankSEOState.search) {
      // Don't make another request if it's the same.
      return;
    }
    dispatch({ type: requestRankSEOType, provider, urlPart, search });

    const url = `api/RankSEO?providerName=${provider}&targetUrlPart=${urlPart}&searchQuery=${search}`;
    const response = await fetch(url);
    const rankSEOResult = await response.json();

    dispatch({ type: receiveRankSEOType, provider,  urlPart, search, rankSEOResult });
  }
};

export const reducer = (state, action) => {
  state = state || initialState;

  if (action.type === requestRankSEOType) {
    return {
      ...state,
      urlPart: action.urlPart,
      search: action.search,
      isLoading: true
    };
  }

  if (action.type === receiveRankSEOType) {
    return {
      ...state,
      urlPart: action.urlPart,
      search: action.search,
      rankSEOResult: action.rankSEOResult,
      isLoading: false
    };
  }

  return state;
};
