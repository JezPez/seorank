import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../store/RankSEO';

class SearchAndRankSEO extends Component {
  constructor(props) {
    super(props);
    this.state = {
      urlPart: 'sympli.com', 
      search: 'e-settlements online',
      provider: 'Google AU',
      touched: {
        urlPart: false,
        search: false,
      }
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    // This method is called when the component is first added to the document
    this.fetchData();
  }

  fetchData() {
    this.props.requestRankSEO(this.state.provider, this.state.urlPart, this.state.search);
  }

  handleSubmit(event) {
    event.preventDefault();
    this.fetchData();
  }

  handleChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleBlur = (field) => (evt) => {
    this.setState({
      touched: { ...this.state.touched, [field]: true },
    });
  }

  canBeSubmitted() {
    const errors = getValidationErrors(this.state.urlPart, this.state.search);
    return !Object.keys(errors).some(x => errors[x]);
  }

  mapStateToProps = state => {
    return { todos: state.todos };
  };

  render() {
    const errors = getValidationErrors(this.state.urlPart, this.state.search);
    const isDisabled = Object.keys(errors).some(x => errors[x]);

    const shouldMarkError = field => {
      const hasError = errors[field];
      const shouldShow = this.state.touched[field];

      return hasError ? shouldShow : false;
    };

    return (
      <div>
        <form>
          <div>
            <div className="form-group">
            <label>The search provider</label>
              <select class="form-control" 
                name="provider" defaultValue={this.state.provider}
                onChange={this.handleChange}>
                {/* TODO: Retrive options with API instead of statically providing them here */}
                <option value="Google AU">Google AU</option>
                <option value="Bing">Bing</option>
              </select>
            </div>
          </div>
          <div className="form-group">
            <label >Your Url to find in search results</label>
            <input name="urlPart"
              type="text"
              className={shouldMarkError("urlPart") ? "form-control is-invalid" : "form-control"}
              id="inputUrlPart"
              aria-describedby="urlPartHelp"
              defaultValue={this.state.urlPart}
              // If deployed with IIS the default max query length is 2048, so 2000 is a practical limit.
              maxLength="2000"
              onChange={this.handleChange}
              onBlur={this.handleBlur("urlPart")} />
            <small id="urlPartHelp" className="form-text text-muted">Can be a full url or just part of one. Eg: 'website' will rank 'website.com' and also 'news.com/articles/aboutwebsite'</small>
          </div>
          <div className="form-group">
            <label >Search</label>
            <input name="search"
              type="text"
              className={shouldMarkError("search") ? "form-control is-invalid" : "form-control"}
              id="inputSearchTerms"
              defaultValue={this.state.search}
              maxLength="2000"
              onChange={this.handleChange}
              onBlur={this.handleBlur("search")} />
            <small id="searchHelp" className="form-text text-muted">Just like a normal web search. Put keywords here.</small>
          </div>
          <button type="submit" className="btn btn-primary" disabled={isDisabled} onClick={this.handleSubmit}>Search and Rank Results</button>
        </form>
        <p />
        {renderRankSEOResultTable(this.props)}
        <p>{this.props.isLoading ? <span>Loading...</span> : []}</p>
      </div>
    );
  }
}

function getValidationErrors(urlPart, search) {
  // We allow for any special characters but the url part must have at least one alpha-numeric char.
  const regExAlphaNum = /[a-zA-Z0-9]/;
  return {
    urlPart: urlPart.length === 0 || !regExAlphaNum.test(urlPart),
    search: search.length === 0
  };
}

function createMarkup(link) {
  return { __html: link };
}

function renderRankSEOResultTable(props) {
  return (
    <div>
      <a href={props.rankSEOResult.initialRequest}>{props.rankSEOResult.initialRequest}</a>
      <p/>
      <table className='table table-striped'>
        <thead>
          <tr>
            <th>Rank</th>
            <th>Link</th>
          </tr>
        </thead>
        <tbody>
          {props.rankSEOResult.rankedSites.map(site =>
            <tr key={site.position}>
              <td>{site.position}</td>
              <td dangerouslySetInnerHTML={createMarkup(site.linkHtml)} />
            </tr>
          )}
        </tbody>
      </table>
    </div>
  );
}

export default connect(
  state => state.rankSEOState,
  dispatch => bindActionCreators(actionCreators, dispatch)
)(SearchAndRankSEO);

