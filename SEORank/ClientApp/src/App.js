import React from 'react';
import { Route } from 'react-router';
import Layout from './components/Layout';
import SearchAndRankSEO from './components/SearchAndRankSEO';

export default () => (
  <Layout>
    <Route exact path='/' component={SearchAndRankSEO} />
  </Layout>
);
