namespace SEORank.Models {
    public class SearchProvider {
        public string Name { get; set; }
        public int ResultsMax { get; set; }
        public string BaseAddress { get; set; }
        public string QueryStart { get; set; }
        public string ResultsStartWith { get; set; }
        public string ResultsEndWith { get; set; }
        public string IndexQueryParam { get; set; }
        public string CaptchaDetect { get; set; }
    }
}