using System.Collections.Generic;

namespace SEORank.Models {
    public class SEORankConfig {
        public List<SearchProvider> SearchProviders {get;set;}
    }
}