using System.Collections.Generic;

namespace SEORank.Models
{
    public class SearchResult
    {
        public List<string> Links { get; set; }
        public string Error { get; set; }
    }
}