using System.Collections.Generic;

namespace SEORank.Models
{
    public class ValidationResult
    {
        public bool IsValid { get; set; } = true;
        public string Error { get; set; }

        public ValidationResult()
        {
        }
        public ValidationResult(string error)
        {
            this.IsValid = false;
            this.Error = error;
        }
    }
}