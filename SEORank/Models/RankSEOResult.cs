using System.Collections.Generic;

namespace SEORank.Models
{
    public class RankSEOResult
    {
        public List<Site> RankedSites { get; set; }
        public string InitialRequest { get; set; }
        public string Error { get; set; }
    }
}