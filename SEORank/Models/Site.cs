namespace SEORank.Models
{
    public class Site
    {
        public int Position { get; set; }
        public string LinkHtml { get; set; }
    }
}