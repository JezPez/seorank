
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using SEORank.Models;

namespace SEORank.Services
{
    public class SearchService : ISearchService
    {
        private readonly HttpClient _client;
        private readonly SEORankConfig _config;

        public SearchService(SEORankConfig config)
        {
            _config = config;

            // If this was to have more than one service, HttpClient would be made to be a singleton.
            _client = new HttpClient();

            // Adding a UserAgent mitigates the issue of being detected as a bot
            _client.DefaultRequestHeaders.UserAgent.ParseAdd("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36");
        }

        public string GetInitialRequest(string providerName, string searchQuery)
        {
            var provider = this.GetSearchProviders().FirstOrDefault(x => x.Name == providerName);
            return GetInitialRequest(provider, searchQuery);
        }

        public async Task<List<Site>> SearchAndRankSites(string providerName, string targetUrlPart, string searchQuery)
        {
            var provider = this.GetSearchProviders().FirstOrDefault(x => x.Name == providerName);
            var searchResult = await this.SearchAndExtractLinks(provider, searchQuery);

            if (!string.IsNullOrEmpty(searchResult.Error))
            {
                // As we're showing the "url did not rank" message below, it makes sense to also show error messages in a similar fashion.
                return new List<Site> { new Site { Position = 0, LinkHtml = $"<span>{searchResult.Error}</span>" } };
            }

            var rankedResults = new List<Site>();
            for (int i = 0; i < searchResult.Links.Count - 1; i++)
            {
                var result = searchResult.Links[i];
                if (result.Contains(targetUrlPart))
                {
                    rankedResults.Add(new Site { Position = i + 1, LinkHtml = result }); // Position like 1st, 2nd, so 0 is 1st
                }
            }

            if (rankedResults.Count == 0)
            {
                rankedResults.Add(new Site { Position = 0, LinkHtml = $"<span>The url did not rank in the first {provider.ResultsMax} results.</span>" });
            }

            return rankedResults;
        }

        private async Task<SearchResult> SearchAndExtractLinks(SearchProvider provider, string searchQuery, List<string> results = null)
        {
            if (results == null)
                results = new List<string>();

            var request = GetInitialRequest(provider, searchQuery) + provider.IndexQueryParam + results.Count;
            var httpResult = await _client.GetAsync(request);
            var content = await httpResult.Content.ReadAsStringAsync();
            var resultsFromThisPage = this.ExtractSearchResultParts(content, provider.ResultsStartWith, provider.ResultsEndWith);
            if (results.Count == 0 && (resultsFromThisPage == null || resultsFromThisPage.Count == 0))
            {
                if (!string.IsNullOrEmpty(provider.CaptchaDetect) && content.Contains(provider.CaptchaDetect))
                {
                    return new SearchResult { Error = $"Captcha was detected and the search provider needs human proof before continuing. Please wait a few minutes then try again." };
                }

                return new SearchResult { Error = $"No results were found with the request {request}." };
            }
            results.AddRange(resultsFromThisPage);
            if (results.Count < provider.ResultsMax && resultsFromThisPage.Count > 0)
            {
                // Recursively find results until hitting the ResultsMax
                await SearchAndExtractLinks(provider, searchQuery, results);
            }
            return new SearchResult { Links = results.Take(provider.ResultsMax).ToList() };
        }

        private List<string> ExtractSearchResultParts(string source, string startsWith, string endsWith)
        {
            // Skip the first because that is all the html before the first result.
            var splits = source.Split(startsWith)?.Skip(1);
            if (splits == null)
            {
                return null;
            }

            var results = new List<string>();
            foreach (var externalLink in splits)
            {
                var endsWithIndexEnd = externalLink.IndexOf(endsWith) + endsWith.Length;
                results.Add(startsWith + externalLink.Substring(0, endsWithIndexEnd));
                // If we had access to 3rd party libraries, I would have added HtmlAgilityPack and managed the tags here in a safer way
                // instead of just getting the whole <a> html and running it in the front end.
                // Also, I deliberately avoided using regex to extract the links. 
                // That is because I find that a majority of devs cannot read regex and therefore it is impractical to maintain.
                // This is a stylistic choice, I have nothing against regex personally, I just favour readability over abstraction.
            }
            return results;
        }

        private string GetInitialRequest(SearchProvider provider, string searchQuery)
        {
            return provider.BaseAddress + provider.QueryStart + searchQuery;
        }

        private List<SearchProvider> GetSearchProviders()
        {
            if (_config.SearchProviders.Any())
            {
                return _config.SearchProviders;
            }
            else
            {
                // Default is google.com.au
                return new List<SearchProvider> { new SearchProvider {
                  Name = "Google AU",
                  ResultsMax = 100,
                  BaseAddress = "https://www.google.com.au/",
                  QueryStart = "search?num=100&q=",
                  ResultsStartWith = "<h3 class=\"r\"><a href=\"/url?q=",
                  ResultsEndWith = "</a></h3>",
                  IndexQueryParam = "&start="
                }};
            }
        }
    }
}