
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using SEORank.Models;

namespace SEORank.Services
{
    public interface ISearchService
    {
        Task<List<Site>> SearchAndRankSites(string providerName, string targetUrlPart, string searchQuery);
        string GetInitialRequest(string providerName, string searchQuery);
    }
}