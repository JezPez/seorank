using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SEORank.Models;
using SEORank.Services;

namespace SEORank.Tests
{
    [TestClass]
    public class SearchServiceTests
    {
        private readonly SearchService _target;
        public SearchServiceTests()
        {
            // TODO use Moq and mock the Http Client
            _target = new SearchService(null);
        }

        // Uncomment this test to make sample response files in the running directory.
        // Use those files in tests by moving them to the Samples folder,
        // Then add them as EmbeddedResource in the SEORank.Tests.csproj
        //[TestMethod]
        public void SearchSampleMaker()
        {
            var searchUrl = "https://www.google.com.au/search?num=10&q=";
            var searchQuery = "lyrics to around the world";
            var client = new HttpClient();
            // Adding a UserAgent mitigates the issue of being detected as a bot
            client.DefaultRequestHeaders.UserAgent.ParseAdd("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36");
            var httpResult = client.GetAsync(searchUrl + searchQuery).GetAwaiter().GetResult();
            // TODO parse results to find targetUrlPart, Make HtmlParser
            var content = httpResult.Content.ReadAsStringAsync().GetAwaiter().GetResult();
            File.WriteAllText($"testData_{searchQuery}.txt", content);
        }
    }
}
