using Microsoft.VisualStudio.TestTools.UnitTesting;
using SEORank.Controllers;
using SEORank.Models;
using SEORank.Services;

namespace SEORank.Tests
{
    [TestClass]
    public class RankSEOControllerTests
    {
        private readonly RankSEOController _controller;
        public RankSEOControllerTests()
        {
            // TODO use Moq
            _controller = new RankSEOController(null);
        }

        [DataTestMethod]
        [DataRow("", "", "")]
        [DataRow(null, null, null)]
        [DataRow("", "sympli", "search")]
        [DataRow("Google AU", "", "search")]
        [DataRow("Google AU", "sympli", "")]
        public void RankSEO_Get_ValidatesInput(string providerName, string targetUrlPart, string searchQuery)
        {
            var result = _controller.GetSEORank(providerName, targetUrlPart, searchQuery);
            Assert.IsFalse(string.IsNullOrEmpty(result.Result.Error));
        }
    }
}
