# SEO Rank

The SEO Ranking tool! 
Made as a technical assessment for [Sympli](https://www.sympli.com.au/) by Jesse Perry

## Running this ASP.Net Core app with React/Redux:
Navigate a terminal or cmd to the same directory as this README and run:
```dotnet run -p SEORank``` 

This will build and run the Asp.NET Core app on port localhost:5001 and serve the React client app too.

Open a browser at [localhost:5001](https://localhost:5001).
You will see a browser-specific security warning because the certificates used to build this app are not yet trusted by your machine. Go through to the site anyway, or to trust the dev certificates on your machine, you can run:
```dotnet dev-certs https --trust```
Then reload localhost in a new tab.

To run the C# unit tests run:
```dotnet test```

Note: This app was built upon the template provided through `dotnet new reactredux`

## Comments about this test
I hope you enjoy browsing the code! 
I know this test could be done fully in js react/redux but I thought I should take the opportunity to show some skills in dotnetcore and some solution architecture principles like splitting services/controllers and unit tests.
Sadly, I ran out of time for Extension 1 and also to get a respectable unit test coverage.  

## Extension 1
On my machine I triggered Google's bot detection captcha so instead of being blocked I thought I should move onto Extension 2 before finishing this.
Unfortunately I got too carried away with the Bing extension and was not able to complete the caching in time. I have used redis cache in the past, and here I was intending to use the regular InMemoryCache here.

## Extension 2
All search providers can be added through a config file. Specifically the SEORank/appsettings.json. You don't even need to recompile the code!

## Extension 3

# Performance
Performance is mostly reliant on the search provider used. I have used Googles custom search API before and it is as-fast as their regular Google search but the more important factor is that the results vary
from what you see with a regular google search and [they admit this](https://support.google.com/customsearch/answer/70392?hl=en). 
This is a react/redux app with a single controller being served from ASP.Net directly. This means that if the rearch results are processing, then the client page will also get served more slowly for other users.
To improve this performance for concurrent users I would consider hosting the published client on cloudfront CDN or another CDN and split the Asp.Net Core API into a different instance (EC2 if using AWS).
This way the static client files are caches around the world where necessary to load quickly while the API processing the searches could be load balanced and scaled out to many instances.

Another option is to convert the API and single controllers Get method into an AWS Lambda or Azure Function so that there is no API image to keep up which would reduce costs (The cache would have to be running elsewhere too).

# Availability
Right now this solution is only to be deployed on the local machine it was built on. Given that we would be paying for uptime/processing for this tool, that means we would be paying for its use for people outside the company too.
We could host it and make it only available to the local office internet with a security policy applied in Azure or AWS. Another option is to build a login screen so only staff can use it.
Or of course just make it a free tool! 
To make it availble as close as possible to 100% I would suggest we deploy at least 2 running API's in parallel so we are able to manage traffic and deployments with a load balancer and we are never left with one offline.
The client side could be served with a CDN like Cloudfront.

# Reliability
Well, bot detection is the biggest risk to reliability. Or a change in the bot detection logic (today it seems to be resolved by specifying a browsers User Agent).
Less frequent requests through caching would mitigate this, but without knowing the method for providers bot detection, it's not certain.
We essentially need to detect the captcha and open an iFrame with the rendered page within that is asking to solve the captcha so that a person may proceed. 
Or find out how Googles bot detection works and don't trigger it in the first place.
Or use Google Custom Search and allow the search results to be slightly different from a natural search.

A bigger reliability concern with this method is the lack of guaranteed support from search providers. When you use an API from a company, there is usually a support guarantee in place to ensure the API will not break for a number of years until they sunset that particular version. 
Parsing the text of a search does not have the same guarantee. Also some safer logic could be used by parsing the html tags instead of direct text (with HtmlAgilityPack).
If Google or Bing were to change their design signifigantly tomorrow then our system wouldn't be able to know what a search result is anymore.
Luckily it is just a config change to update the results start/end parts again.

To increase reliability in this regard I would also add a feature to cache the configurations and make a UI to users can add or edit their own providers without a developers help to manage the config files, then cache those configs which were successful.
That way when it is fixed for someone it is available for everyone.

Thanks for your consideration. Please ask any questions at 1jesseperry@gmail.com :)